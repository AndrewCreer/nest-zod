import { NestFactory } from "@nestjs/core";
import {
  FastifyAdapter,
  NestFastifyApplication,
} from "@nestjs/platform-fastify";
import { AppModule } from "./app.module";

import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";

import { patchNestjsSwagger } from "@abitia/zod-dto";

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  );
  patchNestjsSwagger();

  const options = new DocumentBuilder()
    .setTitle("The Goose Olympics")
    .setDescription("The goose API description")
    .setVersion("1.0")
    .addTag("geese", "geese description")
    //.addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup("_docs", app, document);

  await app.listen(7654);
}
bootstrap();
