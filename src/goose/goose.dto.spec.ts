import { GooseDto, gooseDtoSchema } from "./goose.dto";
import { ZodError } from "zod";

describe("GooseDto", () => {
  it("should be defined", () => {
    expect(new GooseDto()).toBeDefined();
  });

  it("should validate a good goose", () => {
    expect(
      gooseDtoSchema.parse({
        id: "12345678-1234-4567-2222-123456789012",
        email: "Greame.Goose@goose.com",
        number: 99,
        name: "Greame Goose",
      }),
    ).toBeTruthy();
  });

  it("Zod picks up a bad email", () => {
    expect(() =>
      gooseDtoSchema.parse({
        id: "12345678-1234-4567-2222-123456789012",
        email: "Greame.Goose@goose",
        number: 99,
        name: "Greame Goose",
      }),
    ).toThrowError(ZodError);
  });
});
