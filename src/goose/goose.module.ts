import { Module } from "@nestjs/common";
import { GooseController } from "./goose.controller";
import { GooseService } from "./goose.service";

@Module({
  controllers: [GooseController],
  providers: [GooseService],
})
export class GooseModule {}
