import { Test, TestingModule } from "@nestjs/testing";
import { GooseService } from "./goose.service";

describe("GooseService", () => {
  let service: GooseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GooseService],
    }).compile();

    service = module.get<GooseService>(GooseService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
