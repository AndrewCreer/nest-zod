import { Test, TestingModule } from "@nestjs/testing";
import { GooseController } from "./goose.controller";

describe("GooseController", () => {
  let controller: GooseController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GooseController],
    }).compile();

    controller = module.get<GooseController>(GooseController);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
