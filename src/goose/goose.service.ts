import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { CreateGooseDto, GooseDto } from "./goose.dto";
import { v4 } from "uuid";

@Injectable()
export class GooseService {
  private readonly geese: GooseDto[] = [];

  async create(newGoose: CreateGooseDto): Promise<GooseDto> {
    const cat = { ...newGoose, id: v4() };
    if (this.geese.find((g) => g.number == newGoose.number)) {
      throw new HttpException(
        { message: `Goose number:${newGoose.number} already exists` },
        HttpStatus.BAD_REQUEST,
      );
    }
    this.geese.push(cat);
    return cat;
  }

  async findAll(): Promise<GooseDto[]> {
    return this.geese;
  }

  async findOne(id: string): Promise<GooseDto> {
    const filtered = this.geese.filter((g) => g.id == id);
    switch (filtered.length) {
      case 0:
        throw new HttpException(
          { message: `Goose ${id} could not be got out of bed` },
          HttpStatus.NOT_FOUND,
        );
      case 1:
        return filtered.at(0);
      default:
        throw new HttpException(
          { message: `too many ${filtered.length} geeses` },
          HttpStatus.NOT_FOUND,
        );
    }
  }
}
