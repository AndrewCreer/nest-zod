import { Controller, UsePipes, Post, Body, Param, Get } from "@nestjs/common";
import { ZodValidationPipe } from "@abitia/zod-dto";
import { CreateGooseDto, GooseDto } from "./goose.dto";
import { GooseService } from "./goose.service";
import { ApiResponse } from "@nestjs/swagger";

@Controller("/goose")
@UsePipes(ZodValidationPipe)
export class GooseController {
  constructor(private gooseService: GooseService) {}

  @Post()
  public createGoose(@Body() newGoose: CreateGooseDto) {
    return this.gooseService.create(newGoose);
  }

  @Get()
  public async findAll(): Promise<GooseDto[]> {
    return this.gooseService.findAll();
  }

  @Get(":id")
  @ApiResponse({ type: GooseDto, status: 200 })
  public async findOne(@Param("id") id: string): Promise<GooseDto> {
    return this.gooseService.findOne(id);
  }
}
