import z from "zod";
import { createZodDto } from "@abitia/zod-dto";

export const createGooseDtoSchema = z.object({
  name: z.string().min(4).max(30).regex(/^G/),
  number: z.number(),
  email: z.string().email(),
});

export class CreateGooseDto extends createZodDto(createGooseDtoSchema) {}

export const gooseDtoSchema = createGooseDtoSchema.merge(
  z.object({
    id: z.string().uuid(),
  }),
);

export class GooseDto extends createZodDto(gooseDtoSchema) {}
